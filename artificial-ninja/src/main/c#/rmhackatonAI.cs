#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.NinjaScript.Indicators;
using NinjaTrader.NinjaScript.DrawingTools;
using System.Net;
using System.IO;
#endregion

//This namespace holds Strategies in this folder and is required. Do not change it. 
namespace NinjaTrader.NinjaScript.Strategies
{
	public class rmhackatonAI : Strategy
	{
		protected override void OnStateChange()
		{
			if (State == State.SetDefaults)
			{
				Description									= @"main strategy based on java AI";
				Name										= "rmhackatonAI";
				Calculate									= Calculate.OnBarClose;
				EntriesPerDirection							= 1;
				EntryHandling								= EntryHandling.AllEntries;
				IsExitOnSessionCloseStrategy				= false;
				ExitOnSessionCloseSeconds					= 30;
				IsFillLimitOnTouch							= false;
				MaximumBarsLookBack							= MaximumBarsLookBack.TwoHundredFiftySix;
				OrderFillResolution							= OrderFillResolution.Standard;
				Slippage									= 0;
				StartBehavior								= StartBehavior.WaitUntilFlat;
				TimeInForce									= TimeInForce.Gtc;
				TraceOrders									= false;
				RealtimeErrorHandling						= RealtimeErrorHandling.StopCancelClose;
				StopTargetHandling							= StopTargetHandling.PerEntryExecution;
				BarsRequiredToTrade							= 250;
				// Disable this property for performance gains in Strategy Analyzer optimizations
				// See the Help Guide for additional information
				IsInstantiatedOnEachOptimizationIteration	= true;
				delimiter									= ";";
				SMAFast										= 23;
				SMASlow										= 239;
				EMAFast										= 13;
				EMASlow										= 111;
				RSIperiod									= 24;
				RSIsmooth									= 10;				
				MomentumPeriod								= 24;
				DEMAperiod									= 24;

			}
			else if (State == State.Configure)
			{
			}
		}

		protected override void OnBarUpdate()
		{
			//creating call request
			string linia = "";
			linia = linia + delimiter + Close[0].ToString();
			linia = linia + delimiter + SMA(SMAFast)[0].ToString();
			linia = linia + delimiter + SMA(SMASlow)[0].ToString();
			linia = linia + delimiter + EMA(EMAFast)[0].ToString();
			linia = linia + delimiter + EMA(EMASlow)[0].ToString();
			linia = linia + delimiter + RSI(RSIperiod,RSIsmooth)[0].ToString();
			linia = linia + delimiter + RSI(RSIperiod,RSIsmooth).Avg[0].ToString();
			linia = linia + delimiter + Momentum(MomentumPeriod)[0].ToString();
			linia = linia + delimiter + DEMA(DEMAperiod)[0].ToString();
			//Print(linia);
			
			//fetching response
			string url = @"http://localhost:8080/artificial-ninja/trade/" + linia;			
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			request.AutomaticDecompression = DecompressionMethods.GZip;

			using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
			using (Stream stream = response.GetResponseStream())
			using (StreamReader reader = new StreamReader(stream))			
			{
				string result = reader.ReadToEnd();
				string[] prob = result.Split(delimiter.ToCharArray());
				
				//string result = "10;60;30";
				//string[] prob = result.Split(delimiter.ToCharArray());	
				
				int probBuy = int.Parse(prob[0]);
				int probSell = int.Parse(prob[1]);
				int probKeep = int.Parse(prob[2]);
				
				if (probBuy>=50) //Buy
				{
					EnterLong(1);
					ExitShort(1);
				}
				else if (probSell>=50) //Sell
				{
					EnterShort(1);
					ExitLong(1);
				}
				else //keep 
				{
					
				}
				
			}
			
			
		}

		#region Properties
		[NinjaScriptProperty]

		
		[Display(Name="delimiter", Order=1, GroupName="Parameters")]
		public string delimiter
		{ get; set; }
		
		[Display(Name="SMAFast", Order=2, GroupName="Parameters")]
		public int SMAFast
		{ get; set; }
		
		[Display(Name="SMASlow", Order=2, GroupName="Parameters")]
		public int SMASlow
		{ get; set; }
		
		[Display(Name="EMAFast", Order=3, GroupName="Parameters")]
		public int EMAFast
		{ get; set; }
		
		[Display(Name="EMASlow", Order=3, GroupName="Parameters")]
		public int EMASlow
		{ get; set; }
		
		[Display(Name="RSIperiod", Order=4, GroupName="Parameters")]
		public int RSIperiod
		{ get; set; }
		
		[Display(Name="RSIsmooth", Order=4, GroupName="Parameters")]
		public int RSIsmooth
		{ get; set; }
		
		[Display(Name="MomentumPeriod", Order=6, GroupName="Parameters")]
		public int MomentumPeriod
		{ get; set; }
		
		[Display(Name="DEMAperiod", Order=6, GroupName="Parameters")]
		public int DEMAperiod
		{ get; set; }
		
		
		#endregion

	}
}
