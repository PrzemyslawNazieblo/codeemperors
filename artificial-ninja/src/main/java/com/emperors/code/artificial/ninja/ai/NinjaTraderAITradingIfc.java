package com.emperors.code.artificial.ninja.ai;

import java.util.List;

import com.emperors.code.artificial.ninja.model.TradeData;

public interface NinjaTraderAITradingIfc {

	public void feed(List<TradeData> learningInput);
	
	public String trade(List<String> tradingInput);
}
