package com.emperors.code.artificial.ninja.csv.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.emperors.code.artificial.ninja.csv.IndicatorsCSVServiceIfc.CSV_LOCATION;

public class WriterProvider {

    private static BufferedWriter writer;

    public static BufferedWriter getWriter() {
        if(writer == null) {
            instantiateWriter();
        }
        return writer;
    }

    private static void instantiateWriter() {
        File file = new File(CSV_LOCATION);
        try {
            writer = new BufferedWriter(new FileWriter(file, true));
        } catch (IOException e) {
            closeWriter();
            e.printStackTrace();
        }
    }

    public static void closeWriter() {
        if(writer != null) {
            try {
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
