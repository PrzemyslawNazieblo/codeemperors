package com.emperors.code.artificial.ninja.model;

import com.emperors.code.artificial.ninja.enums.TradePredictionClassification;

public class TradeData {
	
//	date	close	SMAFast(23)	SMASlow(239)	EMAFast(13)	EMASlow(111)	RSI(24,10)	RSIavg	Momentum(24)	DEMA(24)
	public static final int DATE_INDEX = 0;
	public static final int CLOSE_INDEX = 1;
	public static final int SMAFAST_INDEX = 2;
	public static final int SMASLOW_INDEX = 3;
	public static final int EMAFAST_INDEX = 4;
	public static final int EMASLOW_INDEX = 5;
	public static final int RSI_INDEX = 6;
	public static final int RSIAVG_INDEX = 7;
	public static final int MOMENTUM_INDEX = 8;
	
	private String date;
	private long close;
	private long smaFast;
	private long smaSlow;
	private long emaFast;
	private long emaSlow;
	private long rsi;
	private long rsiAvg;
	private long momentum;
	private TradePredictionClassification classification;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public long getClose() {
		return close;
	}
	public void setClose(long close) {
		this.close = close;
	}
	public long getSmaFast() {
		return smaFast;
	}
	public void setSmaFast(long smaFast) {
		this.smaFast = smaFast;
	}
	public long getSmaSlow() {
		return smaSlow;
	}
	public void setSmaSlow(long smaSlow) {
		this.smaSlow = smaSlow;
	}
	public long getEmaFast() {
		return emaFast;
	}
	public void setEmaFast(long emaFast) {
		this.emaFast = emaFast;
	}
	public long getEmaSlow() {
		return emaSlow;
	}
	public void setEmaSlow(long emaSlow) {
		this.emaSlow = emaSlow;
	}
	public long getRsi() {
		return rsi;
	}
	public void setRsi(long rsi) {
		this.rsi = rsi;
	}
	public long getRsiAvg() {
		return rsiAvg;
	}
	public void setRsiAvg(long rsiAvg) {
		this.rsiAvg = rsiAvg;
	}
	public long getMomentum() {
		return momentum;
	}
	public void setMomentum(long momentum) {
		this.momentum = momentum;
	}
	public TradePredictionClassification getClassification() {
		return classification;
	}
	public void setClassification(TradePredictionClassification classification) {
		this.classification = classification;
	}
	


}
