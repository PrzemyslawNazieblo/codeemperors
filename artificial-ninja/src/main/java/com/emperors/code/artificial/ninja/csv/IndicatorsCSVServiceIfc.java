package com.emperors.code.artificial.ninja.csv;

import java.util.List;

import com.emperors.code.artificial.ninja.model.TradeData;

public interface IndicatorsCSVServiceIfc {

	public static final String COMMA_DELIMITER = ",";
	public static final String SEMICOLON_DELIMITER = ";";
	public static final String CSV_LOCATION = "C:/tmp/indicators.csv";

	public void addToCSV(String indicators);

	public List<TradeData> loadCSVToList();

	public List<String> splitDelimitedLine(String delimitedLine);

	public void closeFile();

}
