package com.emperors.code.artificial.ninja.ai.rest;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

public interface NinjaTraderRestIfc {
	
	/** Puts single row into CSV
	 * @param feedingVector
	 */
	@GET
    @Path("/addToCSV/{value}")
    @Produces(MediaType.TEXT_PLAIN)
    public void addIndicatorsToCSV(@PathParam("value") final String indicatorsVectorToStore);
	
    /** Feed AI with data from csv
     * @return
     */
    @GET
    @Path("/feed")
    @Produces(MediaType.TEXT_PLAIN)
    public void feedAI();
    
    /** Calculate trade
     * @param tradingVector
     * @return
     */
    @GET
    @Path("/trade/{value}")
    @Produces(MediaType.TEXT_PLAIN)
    public String trade(@PathParam("value") final String tradingVector);

    /**
     * close csv file
     */
    @GET
    @Path("/close")
    @Produces(MediaType.TEXT_PLAIN)
    public void closeFile();
}
