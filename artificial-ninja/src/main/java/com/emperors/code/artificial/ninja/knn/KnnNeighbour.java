package com.emperors.code.artificial.ninja.knn;

import java.util.List;
import java.util.stream.Collectors;

import com.emperors.code.artificial.ninja.enums.TradePredictionClassification;

public class KnnNeighbour {

	private static final int MAX_INDICATORS_SIZE = 6;
	private final List<Long> indicators;
	private TradePredictionClassification classification;
	
	public KnnNeighbour(List<String> indicatorsAsStrings) {
		indicators=indicatorsAsStrings.stream().map(string->Long.parseLong(string)).collect(Collectors.toList());
	}
	

	public long getManhattanDistance(KnnNeighbour other) {
		long distance=0;
		List<Long> otherIndicators = other.getIndicators();
		for(int i=0; i<MAX_INDICATORS_SIZE; i++) {
			distance+= Math.abs(indicators.get(i) - otherIndicators.get(i));
		}
		return distance;
	}

	public List<Long> getIndicators() {
		return indicators;
	}


	public TradePredictionClassification getClassification() {
		return classification;
	}


	public void setClassification(TradePredictionClassification classification) {
		this.classification = classification;
	}
}
