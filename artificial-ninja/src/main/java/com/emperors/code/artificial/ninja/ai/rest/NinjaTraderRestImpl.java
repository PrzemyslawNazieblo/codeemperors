package com.emperors.code.artificial.ninja.ai.rest;

import java.util.List;

import com.emperors.code.artificial.ninja.ai.NinjaTraderAITradingIfc;
import com.emperors.code.artificial.ninja.ai.NinjaTraderAITradingImpl;
import com.emperors.code.artificial.ninja.csv.IndicatorsCSVServiceIfc;
import com.emperors.code.artificial.ninja.csv.IndicatorsCSVServiceImpl;
import com.emperors.code.artificial.ninja.model.TradeData;

import jakarta.ws.rs.Path;

@Path("")
public class NinjaTraderRestImpl implements NinjaTraderRestIfc {

	
	
	private static NinjaTraderAITradingIfc artificialIntelligence = new NinjaTraderAITradingImpl();
	private IndicatorsCSVServiceIfc csvService = new IndicatorsCSVServiceImpl();

	@Override
	public void addIndicatorsToCSV(String indicatorsVectorToStore) {
		csvService.addToCSV(indicatorsVectorToStore);
	}

	@Override
	public void feedAI() {
		
		List<TradeData> tradeDataList = csvService.loadCSVToList();
		artificialIntelligence.feed(tradeDataList);
	}

	@Override
	public String trade(String tradingVector) {
		List<String> tradingInput = csvService.splitDelimitedLine(tradingVector);
		return artificialIntelligence.trade(tradingInput);
	}

	@Override
	public void closeFile() {
		csvService.closeFile();
	}

}
