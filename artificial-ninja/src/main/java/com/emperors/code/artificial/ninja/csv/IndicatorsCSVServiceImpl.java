package com.emperors.code.artificial.ninja.csv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.emperors.code.artificial.ninja.csv.util.WriterProvider;
import com.emperors.code.artificial.ninja.model.TradeData;

public class IndicatorsCSVServiceImpl implements IndicatorsCSVServiceIfc {

	private static int timesUsed = 0;
	private static int USED_THRESHOLD = 10;

	@Override
	public void addToCSV(String indicators) {
		File file = new File(CSV_LOCATION);
		boolean newLine = true;
		if(!file.exists()) {
			newLine = false;
		}
		try {
			BufferedWriter writer = WriterProvider.getWriter();
			if (newLine) {
				writer.newLine();
				writer.append(indicators);
				timesUsed++;
				if(timesUsed > USED_THRESHOLD) {
					timesUsed = 0;
					writer.flush();
				}
			} else
				writer.append(indicators);
		} catch (IOException e) {
		    WriterProvider.closeWriter();
			e.printStackTrace();
		}
	}

	@Override
	public List<TradeData> loadCSVToList() {
		File csvFile = new File(CSV_LOCATION);
		List<TradeData> records = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		        List<String> valuesList = splitDelimitedLine(line);
				records.add(mapCsvRowToTradeData(valuesList));
		    }
		    return records;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private TradeData mapCsvRowToTradeData(List<String> csvTradeRow) {
		TradeData tradeData = new TradeData();
		tradeData.setDate(csvTradeRow.get(TradeData.DATE_INDEX));
		tradeData.setClose(Long.parseLong(csvTradeRow.get(TradeData.CLOSE_INDEX)));
		tradeData.setSmaFast(Long.parseLong(csvTradeRow.get(TradeData.SMAFAST_INDEX)));
		tradeData.setSmaSlow(Long.parseLong(csvTradeRow.get(TradeData.SMASLOW_INDEX)));
		tradeData.setEmaFast(Long.parseLong(csvTradeRow.get(TradeData.EMAFAST_INDEX)));
		tradeData.setEmaSlow(Long.parseLong(csvTradeRow.get(TradeData.EMASLOW_INDEX)));
		tradeData.setRsi(Long.parseLong(csvTradeRow.get(TradeData.RSI_INDEX)));
		tradeData.setRsiAvg(Long.parseLong(csvTradeRow.get(TradeData.RSIAVG_INDEX)));
		return tradeData;
	}

	public List<String> splitDelimitedLine(String line) {
		String[] values = line.split(SEMICOLON_DELIMITER);
		return Arrays.asList(values);
	}

    @Override
    public void closeFile() {
        WriterProvider.closeWriter();
    }

}
