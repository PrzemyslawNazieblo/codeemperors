package com.emperors.code.artificial.ninja.knn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.emperors.code.artificial.ninja.enums.TradePredictionClassification;

public class KNearestNeighboursProcessor {

	private static final int _100_PERCENT = 100;

	private static final int NEIGHBOURS_COUNT = 100;

	private List<KnnNeighbour> trainData = new ArrayList<KnnNeighbour>();

	public void fillTrainData(List<List<String>> learningInput) {
		trainData = learningInput.stream().map(indicatorsAsStrings -> new KnnNeighbour(indicatorsAsStrings))
				.collect(Collectors.toList());
	}

	public NinjaTraderKnnClassifier classify(KnnFeature feature) {
		Set<KnnNeighbour> nNearestNeighbours = getNnearestNeighbours(feature);

		Map<TradePredictionClassification, Long> classForFeature = classifyFromNnearestNeighbours(nNearestNeighbours);
		return new NinjaTraderKnnClassifier(classForFeature);
	}

	private Map<TradePredictionClassification, Long> classifyFromNnearestNeighbours(Set<KnnNeighbour> nNearestNeighbours) {
		Map<TradePredictionClassification, Long> classificationMap = new HashMap<TradePredictionClassification, Long>();
		classificationMap.put(TradePredictionClassification.UP, countClassificationPercentage(nNearestNeighbours, TradePredictionClassification.UP));
		classificationMap.put(TradePredictionClassification.UP, countClassificationPercentage(nNearestNeighbours, TradePredictionClassification.DOWN));
		classificationMap.put(TradePredictionClassification.UP, countClassificationPercentage(nNearestNeighbours, TradePredictionClassification.CONSTANT));
		return classificationMap ;
	}

	private long countClassificationPercentage(Set<KnnNeighbour> nNearestNeighbours, TradePredictionClassification classification) {
		return (nNearestNeighbours.stream().filter(element->element.getClassification()==classification ).count())/_100_PERCENT;
	}

	private Set<KnnNeighbour> getNnearestNeighbours(KnnFeature feature) {
		TreeSet<KnnNeighbour> nNearestNeighbours = new TreeSet<KnnNeighbour>();
		KnnNeighbour firstTrainDataElement = trainData.get(0);
		Long lastNearestNeighbourDistance =feature.getManhattanDistance(firstTrainDataElement);
		nNearestNeighbours.add(firstTrainDataElement);
		for(int i=1; i<trainData.size();i++) {
			KnnNeighbour currentTrainDataElement = trainData.get(i);
			Long manhattanDistance = feature.getManhattanDistance(currentTrainDataElement);
			if(nNearestNeighbours.size()<NEIGHBOURS_COUNT||manhattanDistance<lastNearestNeighbourDistance) {
				nNearestNeighbours.add(currentTrainDataElement);
			}
			if(nNearestNeighbours.size()==NEIGHBOURS_COUNT) {
				nNearestNeighbours.remove(nNearestNeighbours.last());
			}
		}
		return nNearestNeighbours;
	}


}
a
//0. klasyfikacja - dodanie ostatniej kolumny
//zaglądam do wiersza +120 i patrzę na średnią kroczącą z tych okresów EDA
//-czy eda z przyszlosci jest +-1% (parametr). Jesli jest +1,5% to bedzie rosło, jak jest 0 to sie utrzyma, jak minus, to klasyfikujemy, ze bedzie spadalo
//normalizacja
//1. przetwarza -niesk +niesk w -1 + 1
//i arcus tangens
//RSI = RSI/100
//MA=arctan(10x((V-MA)/MA)) V=obecny kurs
//RSIAvg=arctan(10x((RSI-RSIAvg)/RSIAvg)) zeby bylo od 0-1
