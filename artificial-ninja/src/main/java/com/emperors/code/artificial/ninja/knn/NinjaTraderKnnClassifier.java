package com.emperors.code.artificial.ninja.knn;

import java.util.Map;

import com.emperors.code.artificial.ninja.enums.TradePredictionClassification;

public class NinjaTraderKnnClassifier {

	private final Map<TradePredictionClassification, Long> classToPercentage;

	public NinjaTraderKnnClassifier(Map<TradePredictionClassification, Long> classToPercentage) {
		this.classToPercentage = classToPercentage;
	}

	public Map<TradePredictionClassification, Long> getClassToPercentage() {
		return classToPercentage;
	}
}
