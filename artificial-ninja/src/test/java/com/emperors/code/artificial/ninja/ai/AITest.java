package com.emperors.code.artificial.ninja.ai;

import org.junit.Test;

import com.emperors.code.artificial.ninja.ai.rest.NinjaTraderRestIfc;
import com.emperors.code.artificial.ninja.ai.rest.NinjaTraderRestImpl;

public class AITest {

	@Test
	public void shouldFeedAI() {
		NinjaTraderRestIfc rest = new NinjaTraderRestImpl();
		rest.feedAI();
	}
}
